﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NetGame {
	public class CameraController : MonoBehaviour {
		[SerializeField]
		private PhotonView _photonView;
		public Transform Target;

		private void Update() {
			if(Target == null) return;

			transform.position = new Vector3(Target.position.x, transform.position.y, Target.position.z - 13f);
		}
	}
}