﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace NetGame {
	public class ProjectileController : MonoBehaviourPunCallbacks {
		[SerializeField, Range(1f, 10f)]
		private float _moveSpeed = 3f;
		[SerializeField, Range(1f, 10f)]
		private float _damage = 1f;
		[SerializeField, Range(1f, 15f)]
		private float _lifeTime = 7f;

		public float GetDamage => _damage;
		public string ParentName { get; set; }

		private void Start() {
			StartCoroutine(OnDie());
		}

		private void FixedUpdate() {
			transform.position += transform.forward * _moveSpeed * Time.fixedDeltaTime;
		}

		private IEnumerator OnDie() {
			yield return new WaitForSeconds(_lifeTime);
			if(isActiveAndEnabled) {
				PhotonNetwork.Destroy(gameObject);
			}
		}
	}
}