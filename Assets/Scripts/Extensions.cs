﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace NetGame {
	public static class Debugger {
		private static Text _console;
		
		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
		public static void OnStart() {
			_console = GameObject.Find("console").GetComponent<Text>();
		}

		public static void Log(string message) {
#if UNITY_EDITOR
			Debug.Log(message);
#elif UNITY_STANDALONE && !UNITY_EDITOR
			_console.text += "\n" + message;
#endif
		}
	}
}