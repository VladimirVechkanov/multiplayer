﻿using ExitGames.Client.Photon;
using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace NetGame {
	public class GameManager : MonoBehaviourPunCallbacks {
		private PlayerController _player1;
		private PlayerController _player2;

		[SerializeField]
		private GameObject WinLosePanel;
		[SerializeField]
		private Text WinLoseText;
		[SerializeField]
		private PhotonView _photonView;

		private float _quitDelay = 2f;
		[SerializeField]
		private string _playerPrefabName = "Player";
		[SerializeField, Range(1f, 15f)]
		private float _randomInterval = 7f;

		private void Start() {
			var pos = new Vector3(Random.Range(-_randomInterval, _randomInterval), 2f, Random.Range(-_randomInterval, _randomInterval));
			var go = PhotonNetwork.Instantiate(_playerPrefabName + PhotonNetwork.NickName, pos, new Quaternion());
		}

		private void Update() {
			if(Input.GetKey(KeyCode.Escape)) {
				_quitDelay -= Time.deltaTime;

				if(_quitDelay <= 0f) {
					PhotonNetwork.LeaveRoom();
				}
			}
			else {
				_quitDelay = 2f;
			}
		}

		public override void OnLeftRoom() {
			SceneManager.LoadScene(0);
		}

		public void AddPlayer(PlayerController player) {
			if(player.name.Contains("1")) {
				_player1 = player;
			}
			else {
				_player2 = player;
			}

			if(_player1 != null && _player2 != null) {
				_player1.SetTarget(_player2.transform);
				_player2.SetTarget(_player1.transform);
			}
		}
	}
}