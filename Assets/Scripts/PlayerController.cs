﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NetGame {
	public class PlayerController : MonoBehaviourPunCallbacks, IPunObservable {
		private Rigidbody _rigidbody;
		private GameManager _gameManager;
		private bool _endGame = false;
		private PlayerController _opponent;
		private GameObject WinLosePanel;
		private Text WinLoseText;
		private CameraController _camera;

		private Transform _target;
		[SerializeField]
		private Transform _spawnBullet;
		[SerializeField]
		private Slider _sliderHP;
		[SerializeField]
		private Image _imageHP;

		[Space, SerializeField, Range(1f, 100f)]
		private float _moveSpeed = 2f;
		[SerializeField, Range(0.5f, 5f)]
		private float _maxSpeed = 2f;
		[Range(1f, 100f)]
		public float Health = 5f;

		[Space, SerializeField, Range(0.1f, 1f)]
		private float _attackDelay = 0.5f;
		[SerializeField, Range(0.1f, 1f)]
		private float _rotateDelay = 0.25f;

		[Space, SerializeField]
		private PhotonView _photonView;

		private void Start() {
			_rigidbody = GetComponent<Rigidbody>();

			_gameManager = FindObjectOfType<GameManager>();
			_gameManager.AddPlayer(this);

			if(_photonView.IsMine) {
				_camera = FindObjectOfType<CameraController>();
				_camera.Target = transform;

				WinLosePanel = GameObject.Find("WinLosePanel");
				WinLoseText = GameObject.Find("WinLoseText").GetComponent<Text>();
				WinLosePanel.SetActive(false);
			}

			_sliderHP.maxValue = Health;
			_sliderHP.value = Health;
		}

		private IEnumerator WinLose() {
			while(true) {
				if(_photonView.IsMine && !_endGame) {
					if(Health <= 0f) {
						WinLosePanel.SetActive(true);
						WinLoseText.text = "You LOSE!";
						StartCoroutine(OnWinLose());
						_endGame = true;
					}
					else if(_opponent != null && _opponent.Health <= 0f) {
						WinLosePanel.SetActive(true);
						WinLoseText.text = "You WIN!";
						StartCoroutine(OnWinLose());
						_endGame = true;
					}
				}

				yield return new WaitForSeconds(0.1f);
			}
		}

		private IEnumerator OnWinLose() {
			yield return new WaitForSeconds(3f);
			PhotonNetwork.LeaveRoom();
		}

		private void FixedUpdate() {
			if(!_photonView.IsMine || _endGame) return;

			_sliderHP.transform.LookAt(_camera.transform);

			var directionX = Input.GetAxis("Horizontal");
			var directionY = Input.GetAxis("Vertical");

			if(directionX == 0f && directionY == 0f) return;

			var velocity = _rigidbody.velocity;
			velocity += new Vector3(directionX, 0f, directionY) * _moveSpeed * Time.fixedDeltaTime;

			velocity.y = 0f;
			velocity = Vector3.ClampMagnitude(velocity, _maxSpeed);
			_rigidbody.velocity = velocity;
		}

		private void OnTriggerEnter(Collider other) {
			if(_endGame) return;

			var bullet = other.GetComponent<ProjectileController>();
			if(bullet != null && bullet.ParentName != null && bullet.ParentName != name) {
				_photonView.RPC("TakeDamage", RpcTarget.AllBuffered, bullet.GetDamage);
				PhotonNetwork.Destroy(bullet.gameObject);
			}

			if(other.tag == "Obstacle") {
				_photonView.RPC("TakeDamage", RpcTarget.AllBuffered, 10000f);
			}
		}

		[PunRPC]
		public void TakeDamage(float damage) {
			Health -= damage;
			_sliderHP.value = Health;
		}

		private IEnumerator Fire() {
			while(true) {
				var bullet = PhotonNetwork.Instantiate("Bullet", _spawnBullet.position, transform.rotation);
				bullet.transform.LookAt(_target);
				bullet.GetComponent<ProjectileController>().ParentName = name;
				yield return new WaitForSeconds(_attackDelay);
			}
		}

		private IEnumerator Focus() {
			while(true) {
				if(_target != null) {
					transform.LookAt(_target);
					transform.eulerAngles = new Vector3(0f, transform.eulerAngles.y, 0f);
				}
				yield return new WaitForSeconds(_rotateDelay);
			}
		}

		public void SetTarget(Transform target) {
			_target = target;

			_opponent = target.GetComponent<PlayerController>();

			StartCoroutine(WinLose());

			if(!_photonView.IsMine) return;

			StartCoroutine(Fire());
			StartCoroutine(Focus());
		}

		public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info) {
			if(stream.IsWriting) {
				stream.SendNext(Health);
				stream.SendNext(_sliderHP.value);
			}
			else {
				Health = (float)stream.ReceiveNext();
				_sliderHP.value = (float)stream.ReceiveNext();
			}
		}
	}
}